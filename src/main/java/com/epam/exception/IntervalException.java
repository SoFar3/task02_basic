package com.epam.exception;

/**
 * {@code IntervalException} is the exception that can be throw
 * during interval parsing.
 *
 * @author Yaroslav Telpyi
 */
public class IntervalException extends RuntimeException {

    /**
     * Constructs a new runtime exception with the specified detail message.
     * @param message the detail message.
     */
    public IntervalException(String message) {
        super(message);
    }

}
