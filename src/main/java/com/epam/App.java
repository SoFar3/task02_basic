package com.epam;

import java.io.IOException;

/**
 * Main class.
 *
 * @author Yaroslav Teplyi
 */
public class App {

    /**
     *
     */
    private static final int AMOUNT = 20;

    /**
     * Entry point of our app.
     * @param args initial parameters that can be pass to our app
     * @throws IOException throws an exception because of working with IO
     */
    public static void main(String[] args) throws IOException {
        IntervalSequence intervalSequence = new IntervalSequence();
        intervalSequence.consoleInput();

        System.out.print("Even nums: ");
        intervalSequence.printEven();
        System.out.print("Odd nums: ");
        intervalSequence.printOddReverse();

        System.out.println("Sum of odd nums: " + intervalSequence.sumOdd());
        System.out.println("Sum of even nums: " + intervalSequence.sumEven());

        // Fibonacci
        System.out.println("Fibonacci");

        System.out.println("Biggest even num: "
                + FibonacciUtils.maxEven(AMOUNT));

        System.out.println("Biggest odd num: "
                + FibonacciUtils.maxOdd(AMOUNT));

        System.out.println("Percentage of even nums: "
                + FibonacciUtils.evenPercentage(AMOUNT));

        System.out.println("Percentage of odd nums: "
                + FibonacciUtils.oddPercentage(AMOUNT));
    }

}
