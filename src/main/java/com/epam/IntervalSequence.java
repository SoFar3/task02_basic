package com.epam;

import com.epam.exception.IntervalException;
import jdk.nashorn.internal.runtime.regexp.joni.exception.InternalException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.StringTokenizer;
import java.util.stream.IntStream;

/**
 * The {@code IntervalSequence} class contains several
 * useful method for getting statistic about
 * numbers in interval.
 *
 * @author Yaroslav Teplyi
 */
public class IntervalSequence {

    /**
     * An array that contains two values. Represents an interval.
     */
    private int[] interval;

    /**
     * Constructs an empty interval.
     */
    public IntervalSequence() {
        interval = new int[2];
    }

    /**
     * Constructs an interval with given params.
     * @param n first value
     * @param m second value
     */
    public IntervalSequence(final int n, final int m) {
        interval = new int[] {n, m};
    }

    /**
     * Allows the user to enter a interval in the console.
     * @throws IOException throws an exception because of working with IO
     */
    public final void consoleInput() throws IOException {
        BufferedReader br = new BufferedReader(
                new InputStreamReader(System.in, StandardCharsets.UTF_8));

        boolean waitInput = true;
        while (waitInput) {
            try {
                System.out.println("Enter interval[n, m]: ");
                String userInput = br.readLine();
                this.interval = parseInterval(userInput);
                waitInput = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Parses the interval from string to int array.
     * @param str string representation of interval
     * @return valid interval
     * @throws InternalException when user entered incorrect values
     */
    private int[] parseInterval(final String str) throws InternalException {
        int i = 0;
        int[] interval = new int[2];
        StringTokenizer tokenizer = new StringTokenizer(str, ",;[] ");

        if (tokenizer.countTokens() > 2 || tokenizer.countTokens() < 2) {
            throw new IntervalException("Incorrect interval notation syntax. "
                    + "Interval must contain 2 numbers."
                    + "\n Expected: [n, m]\n You entered: [n] / [n, m, ...]");
        }

        while (tokenizer.hasMoreElements()) {
            interval[i++] = Integer.parseInt(tokenizer.nextToken());
        }

        if (interval[0] > interval[1]) {
            throw new IntervalException("Incorrect interval numbers. "
                    + "First interval number must be less than second."
                    + "\n Expected: n < m.\n You entered: n > m");
        }

        return interval;
    }

    /**
     * Adds up odd numbers in interval.
     * @return sum of odd numbers
     */
    public final int sumOdd() {
        return IntStream
                .rangeClosed(interval[0], interval[1])
                .filter(FibonacciUtils.oddFilter())
                .sum();
    }

    /**
     * Adds up even numbers in interval.
     * @return sum of even numbers
     */
    public final int sumEven() {
        return IntStream
                .rangeClosed(interval[0], interval[1])
                .filter(FibonacciUtils.evenFilter())
                .sum();
    }

    /**
     * Prints even numbers in interval.
     */
    public final void printEven() {
        IntStream
                .rangeClosed(interval[0], interval[1])
                .filter(FibonacciUtils.evenFilter())
                .forEach(num -> System.out.print(num + " "));
        System.out.println();
    }

    /**
     * Prints even numbers in interval.
     */
    public final void printOddReverse() {
        IntStream
                .rangeClosed(interval[0], interval[1])
                .filter(FibonacciUtils.oddFilter())
                .boxed()
                .sorted(Collections.reverseOrder())
                .forEach(num -> System.out.print(num + " "));
        System.out.println();
    }

    /**
     * Getter for {@link IntervalSequence#interval}.
     * @return the interval
     */
    public final int[] getInterval() {
        return interval;
    }

    /**
     * Setter for {@link IntervalSequence#interval}.
     * @param interval interval
     */
    public final void setInterval(int[] interval) {
        this.interval = interval;
    }

}
