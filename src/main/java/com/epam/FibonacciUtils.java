package com.epam;

import java.util.function.IntPredicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * The {@code FibonacciUtils} class contains several
 * useful method for getting statistic about
 * fibonacci sequence.
 *
 * @author Yaroslav Teplyi
 */
public final class FibonacciUtils {

    /**
     * 100%.
     */
    private static final int FORMULA_PERCENTAGE = 100;

    /**
     * Private constructor.
     */
    private FibonacciUtils() { }

    /**
     * Returns fibonacci sequence.
     * @param amount of fibonacci numbers
     * @return {@link IntStream} filled in fibonacci numbers
     */
    public static IntStream fibonacci(final int amount) {
        return Stream
                .iterate(new int[] {0, 1},
                        nums -> new int[] {nums[1], nums[0] + nums[1]})
                .limit(amount).mapToInt(nums -> nums[0]);
    }

    /**
     * Returns percentage of odd numbers in fibonacci sequence.
     * @param amount of fibonacci numbers
     * @return percentage of odd numbers
     */
    public static double oddPercentage(final int amount) {
        return ((fibonacci(amount)
                .filter(oddFilter())
                .count() * 1.) / (amount * 1.)) * FORMULA_PERCENTAGE;
    }

    /**
     * Returns percentage of even numbers in fibonacci sequence.
     * @param amount of fibonacci numbers
     * @return percentage of even numbers
     */
    public static double evenPercentage(final int amount) {
        return ((fibonacci(amount)
                .filter(evenFilter())
                .count() * 1.) / (amount * 1.)) * FORMULA_PERCENTAGE;
    }

    /**
     * Returns biggest odd number in fibonacci sequence.
     * @param amount of fibonacci numbers
     * @return max odd number
     */
    public static int maxOdd(final int amount) {
        return fibonacci(amount)
                .filter(oddFilter())
                .max()
                .getAsInt();
    }

    /**
     * Returns biggest even number in fibonacci sequence.
     * @param amount of fibonacci numbers
     * @return max even number
     */
    public static int maxEven(final int amount) {
        return fibonacci(amount)
                .filter(evenFilter())
                .max()
                .getAsInt();
    }

    /**
     * Filter for even numbers.
     * @return {@link IntPredicate} for filtering event numbers
     */
    public static IntPredicate evenFilter() {
        return arg -> arg % 2 == 0;
    }

    /**
     * Filter for even numbers.
     * @return {@link IntPredicate} for filtering odd numbers
     */
    public static IntPredicate oddFilter() {
        return arg -> arg % 2 != 0;
    }

}
